#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

project=mandenkan
file=lexicon.xml

#[ -f ${HEADWORDS} ] || { echo "${HEADWORDS} not found.  Exiting..."; exit 1; }

for x in tmp xml xsd yml adoc json xls; do export "${x}=${project}.$x"; done

for x in jq xq; do
  type -P $x >/dev/null 2>&1 || {
    echo >&2 "${x} not installed.  Aborting."
    exit 1
  }
done

function count_xml {
  # cat $1 | awk -v file=$1 '{n += gsub(/\<ar\>/, "")} END {print n " entries in " file}'
  echo $(sed -n '/<ar>/p' $1 | wc -l) xml headwords in $1
  echo $(sed -n '/<\/ex>/p' $1 | wc -l) xml examples in $1
}

function count_yml {
  echo $(sed -n '/\- k/p' $1 | wc -l) yaml headwords in $1
  echo $(sed -n '/\+@ex-id/p' $1 | wc -l) yaml examples in $1
}

echo $file
xslt3 -xsl:mandenkan-sort.xsl -s:$project/$file >sorted-$tmp
yq -px -oy sorted-$tmp >new-$yml
count_xml "$project/$file"
count_xml sorted-$tmp
count_yml "$yml"
count_yml new-$yml
cp $yml $(date -I)-$yml
rm *.tmp
