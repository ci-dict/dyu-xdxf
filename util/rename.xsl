<?xml version="1.0"?>
<xsl:stylesheet 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
version="1.0">
<xsl:output method="xml" indent="yes" omit-xml-declaration="no"/>

  <!-- IdentityTransform -->
<xsl:template match="/">
  <xsl:apply-templates select="@*|node()"/>
</xsl:template>

  <xsl:template match="/ | @* | node()">
    <xsl:copy>
      <xsl:apply-templates select="@* | node()"/>
    </xsl:copy>
  </xsl:template>

</xsl:stylesheet>
