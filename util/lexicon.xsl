<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
xmlns:xsl  = "http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="no"/>

<!-- IdentityTransform -->
<xsl:template match="/">
  <xsl:apply-templates select="@*|node()"/>
</xsl:template>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

<xsl:variable name="file" select="document('meta_info.xml')" />

<xsl:template match="/">
  <xdxf revision="034">
    <xsl:copy>
      <xsl:copy-of select="$file/node()"/>
    </xsl:copy>
    <xsl:apply-templates select="@* | node()"/>
  </xdxf>
</xsl:template>

<xsl:template match="/lexicon">
  <lexicon>
    <xsl:for-each select="headword">
      <xsl:sort select="dyu" lang="en" data-type="text" order="ascending"/>

      <ar>
        <k xml:lang="dyu"><xsl:value-of select="current()/dyu"/></k>
        <xsl:for-each select="./alt">
          <xsl:if test="current() != ''">
            <k xml:lang="dyu"><xsl:value-of select="."/></k>
          </xsl:if>
        </xsl:for-each>
        <!-- check where to put this comment later; its too long as an attribute -->
        <def freq="{current()/trans/speech/def/note4}">
          <co>
            <xsl:value-of select="current()/trans/detail"/>
          </co>

          <def xml:lang="fr">
            <xsl:for-each select="./trans/speech">

              <xsl:if test="current()/type != 'composé'">
                <def def-id="{t-uuid}">
                  <gr>
                    <abbr><xsl:value-of select="current()/type"/></abbr>
                  </gr>

                  <co>
                    <xsl:value-of select="current()/def/note2"/>
                  </co>

                  <xsl:for-each select="def">
                    <def xml:lang="fr" def-id="{gl-id}">
                      <deftext>
                        <xsl:value-of select="gloss"/>
                      </deftext>

                      <xsl:for-each select="example">

                        <ex type="exm" ex-id="{ex-id}">
                          <ex_orig>
                            <xsl:value-of select="source"/>
                          </ex_orig>
                          <ex_tran>
                            <xsl:value-of select="target"/>
                          </ex_tran>
                        </ex>
                      </xsl:for-each>

                      <etm>
                        <xsl:value-of select="note3"/>
                      </etm>

                      <xsl:for-each select="tags">
                        <categ>
                          <xsl:value-of select="."/>
                        </categ>
                      </xsl:for-each>
                    </def>
                  </xsl:for-each>

                </def>

              </xsl:if>
            </xsl:for-each>
          </def>


          <xsl:if test="(./cf != '') or (./emp != '') or (./alt !='')">
            <xsl:element name="sr">
              <xsl:for-each select="./cf">
                <xsl:if test="current() != ''">
                  <kref type="syn"><xsl:value-of select="."/></kref>
                </xsl:if>
              </xsl:for-each>

              <xsl:if test="./emp != ''">
                <kref type="par"><xsl:value-of select="./emp"/></kref>
              </xsl:if>

              <xsl:for-each select="./alt">
                <xsl:if test="current() != ''">
                  <kref type="spv"><xsl:value-of select="."/></kref>
                </xsl:if>
              </xsl:for-each>
            </xsl:element>
          </xsl:if>
        </def>

      </ar>
    </xsl:for-each>
    <xsl:for-each select="headword/trans/speech">
      <xsl:if test="current()/type = 'composé'">
        <xsl:for-each select="def">
          <ar>
            <k xml:lang="dyu"><xsl:value-of select="note2"/></k>
            <def>
              <co>
              </co>
              <def xml:lang="fr">
                <def def-id="{../t-uuid}">
                  <gr>
                    <abbr>rel</abbr>
                  </gr>
                  <co>
                    <xsl:value-of select="note3"/>
                  </co>
                  <def xml:lang="fr" def-id="{gl-id}">
                    <deftext>
                        <xsl:value-of select="gloss"/>
                    </deftext>

                    <xsl:for-each select="example">
                      <ex type="exm" ex-id="{ex-id}">
                        <ex_orig>
                          <xsl:value-of select="source"/>
                        </ex_orig>
                        <ex_tran>
                          <xsl:value-of select="target"/>
                        </ex_tran>
                      </ex>
                    </xsl:for-each>
                  </def>
                </def>
              </def>
              <xsl:element name="sr">
                <kref type="rel"><xsl:value-of select="ancestor::headword/dyu"/></kref>
              </xsl:element>
            </def>
          </ar>
        </xsl:for-each>
      </xsl:if>
    </xsl:for-each>
  </lexicon>
</xsl:template>

<xsl:template match="/lexicon/headword"/>
<xsl:template match="/lexicon/headword/alt"/>
<xsl:template match="/lexicon/headword/cf"/>
<xsl:template match="/lexicon/headword/dyu"/>
<xsl:template match="/lexicon/headword/emp"/>
<xsl:template match="/lexicon/headword/trans"/>
<xsl:template match="/lexicon/headword/trans/detail"/>
<xsl:template match="/lexicon/headword/trans/lang"/>
<xsl:template match="/lexicon/headword/trans/speech/t-uuid"/>
<xsl:template match="/lexicon/headword/trans/speech/def/tags"/>
<xsl:template match="/lexicon/headword/trans/speech/def/gl-id"/>
<xsl:template match="/lexicon/headword/trans/speech/def/note4"/>
<xsl:template match="/lexicon/headword/trans/speech/def/example/ex-id"/>
</xsl:stylesheet>
