<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
>
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="tab" select="'&#09;'" />
<xsl:param name="pipe" select="'|'" />
<xsl:param name="plus" select="' +'" />
<xsl:param name="double-colon" select="' ::'" />
<xsl:param name="break" select="'&#xA;'" />
<xsl:param name="sep" select="'&#09;'" />
<xsl:param name="eol" select="'&#10;'" />
<xsl:strip-space elements="*" />

<!--
-->
<xsl:template match="lexicon">
  <xsl:copy>
    <xsl:apply-templates>
      <xsl:sort select="k" lang="en"/>
    </xsl:apply-templates>
  </xsl:copy>
</xsl:template>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="@*|node()"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="lexicon">
  <xsl:apply-templates select="k"/>
</xsl:template>

<!--
<xsl:template match="headword">
    <xsl:apply-templates />
    <xsl:if test="following-sibling::*">
      <xsl:value-of select="$eol" />
    </xsl:if>
  </xsl:template>

  <xsl:value-of select="concat('.', $tab)" />
  <xsl:value-of select="concat(normalize-space(),$plus)" />
-->

<!-- remove normalize-space() if you want keep white-space at it is --> 
<xsl:template match="*">
  <xsl:value-of select="concat(normalize-space(.),$tab)" />

  <xsl:if test="position() != last()">
    <xsl:value-of select="$tab"/>
  </xsl:if>

  <xsl:if test="following-sibling::*">
    <xsl:value-of select="$eol" />
  </xsl:if>
</xsl:template>
</xsl:stylesheet>

