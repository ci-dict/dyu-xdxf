
    <!-- t-uuid -->
    <xsl:value-of select="@def-id" />
    <xsl:copy-of select="$CSEP" />

    <!-- dyu -->
    <xsl:if test="gr/abbr = 'composé'" >
      <xsl:value-of select="def[@xml:lang='dyu']/deftext/normalize-space(dtrn)" />
      <xsl:copy-of select="$CSEP" />
    </xsl:if>
    <xsl:if test="gr/abbr != 'composé'" >
      <xsl:value-of select="../../k" separator="|" />
      <xsl:copy-of select="$CSEP" />
    </xsl:if>

    <!-- no source -->
    <xsl:copy-of select="$CSEP" />

    <!-- speech -->
    <xsl:value-of select="gr/abbr" />
    <xsl:copy-of select="$CSEP" />

    <!-- gloss/french consolidate the multiple french defs-->
    <xsl:for-each select="def[@xml:lang='fr']">
      <xsl:value-of select="deftext/normalize-space(dtrn)" separator="|"/>
    </xsl:for-each>
    <xsl:copy-of select="$CSEP" />

    <!-- hint1  is emprunt-->
    <xsl:copy-of select="../sr/kref[@type='par']" />
    <xsl:copy-of select="$CSEP" />

    <!--Alt not used here -->
    <xsl:for-each select="(1 to 4)">
      <xsl:copy-of select="$CSEP" />
    </xsl:for-each>

    <!--emp -->
    <xsl:copy-of select="../sr/kref[@type='par']" />
    <xsl:copy-of select="$CSEP" />

    <!--cf -->
    <xsl:variable name="countCF" select="5"/>
    <xsl:for-each select="../sr/kref[@type='syn']">
      <xsl:if test="position() &lt; $countCF">
        <xsl:value-of select="."/>
        <xsl:copy-of select="$CSEP" />
      </xsl:if>
    </xsl:for-each>

    <xsl:if test="count(../sr/kref[@type='syn']) &lt; 5">
      <xsl:for-each select="(1 to 4 - count(../sr/kref[@type='syn']))">
        <xsl:copy-of select="$CSEP" />
      </xsl:for-each>
    </xsl:if>

    <!--notes 1 to 4 -->
    <xsl:copy-of select="$CSEP" />
    <xsl:copy-of select="$CSEP" />
    <xsl:copy-of select="$CSEP" />
    <xsl:value-of select="../../def/@freq" />
    <xsl:copy-of select="$CSEP" />

    <!--text not available -->
    <xsl:for-each select="(2 to 4)">
      <xsl:copy-of select="$CSEP" />
    </xsl:for-each>

    <!--examples 1 to 4 available -->
    <xsl:for-each select="(1 to 4)">
      <xsl:copy-of select="$CSEP" />
    </xsl:for-each>
