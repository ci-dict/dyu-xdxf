#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

word=$1

yq eval 'select(.lexicon.ar[] | .k.content | contains("$word") or (.sr.kref[] | .content | contains("$word")))' lexicon.yaml

