<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
xmlns:xsl  = "http://www.w3.org/1999/XSL/Transform" 
xmlns:xs   = "http://www.w3.org/2001/XMLSchema">
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="delim" select="','" />
<xsl:param name="header" select="true()" />
<xsl:param name="pipe" select="'|'" />
<xsl:param name="under" select="'_'" />
<xsl:param name="TSEP" select="'&#09;'" />
<xsl:param name="CSEP" select="','" />
<xsl:variable name="RSEP" select="'&#10;'" />  <!-- LF -->

<!-- output tsv file for anki -->

<!-- IdentityTransform -->
<xsl:template match="/xdxf/lexicon">
  <xsl:apply-templates select="@*|node()"/>
</xsl:template>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="/xdxf/lexicon"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="/xdxf/lexicon">
<!-- speech: the main elements -->
  <xsl:variable name="headwords" select="ar" />
  <xsl:variable name="defs" select="$headwords/def/def/def" />
  <xsl:variable name="grs" select="$defs/gr" />

  <!-- Data -->
  <xsl:for-each select="$defs">

    <!-- dyu -->
    <!-- get words only not expressions -->
    <xsl:if test="gr/abbr != 'exp'" >
      <xsl:value-of select="../../../k[1]" />
      <xsl:copy-of select="$TSEP" />
      <xsl:value-of select="../../../k[1]" />
      <xsl:copy-of select="$RSEP" />

      <!-- Alt-->
      <xsl:variable name="countALT" select="count(../../sr/kref[@type='spv'])"/>

      <xsl:for-each select="../../sr/kref[@type='spv']">
        <xsl:value-of select="." />
        <xsl:copy-of select="$TSEP" />
        <xsl:value-of select="../../../k[1]" />
        <xsl:copy-of select="$RSEP" />
      </xsl:for-each>
    </xsl:if>

  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>
