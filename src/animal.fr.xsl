<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xsl:output method="text" encoding="UTF-8" />
  <xsl:param name="header" select="true()" />
  <xsl:param name="CSEP" select="'&#09; '" />
  <xsl:variable name="RSEP" select="'&#10;'" />  <!-- LF -->

  <!-- IdentityTransform -->
  <xsl:template match="/xdxf/lexicon">
    <xsl:apply-templates select="@*|node()" />
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="/xdxf/lexicon" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/xdxf/lexicon">

    <!-- Header. -->
    <xsl:if test="$header">
      <xsl:text>Dioula</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>Français</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>Usage</xsl:text>
      <xsl:copy-of select="$RSEP" />
    </xsl:if>

    <!-- dyu -->
    <xsl:variable name="defs" select="ar/def/def[@xml:lang='fr']/def/def" />
    <xsl:for-each select="$defs">
      <xsl:sort select="../../../../k[1]" />
      <xsl:if test="categ = 'animal'">
        <xsl:value-of select="../../../../k[1]" />
        <xsl:copy-of select="$CSEP" />
        <xsl:value-of select="deftext" />
        <xsl:copy-of select="$CSEP" />
        <xsl:value-of select="ex/ex_orig[1]" />
        <xsl:copy-of select="$RSEP" />
      </xsl:if>
    </xsl:for-each>

  </xsl:template>

</xsl:stylesheet>

