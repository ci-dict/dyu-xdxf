<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema" version="2.0">
  <xsl:output method="text" encoding="UTF-8" />
  <xsl:param name="header" select="true()" />
  <xsl:param name="pipe" select="' | '" />
  <xsl:param name="colon" select="' : '" />
  <xsl:param name="delim" select="','" />
  <xsl:param name="CSEP" select="'&#9;'" />
  <xsl:variable name="RSEP" select="'&#10;'" />

  <!-- LF -->
  <!-- <xsl:variable name="maxExamples" select="max($exampleCount/count/@count) cast as xs:integer"
  />-->
  <xsl:variable name="maxExamples" select="18 cast as xs:integer" />
  <!-- IdentityTransform -->
  <xsl:template match="/xdxf/lexicon">
    <xsl:apply-templates select="@*|node()" />
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="/xdxf/lexicon" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="/xdxf/lexicon">
    <!-- speech: the main elements -->
    <xsl:variable name="headwords" select="ar" />
    <xsl:variable name="defs" select="$headwords/def/def/def" />
    <xsl:variable name="grs" select="$defs/gr" />
    <!-- Header. -->
    <xsl:if test="$header">
      <!-- dyu -->
      <xsl:text>:START_ID</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>:END_ID</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>:TYPE</xsl:text>
      <xsl:copy-of select="$RSEP" />
    </xsl:if>
    <!-- Data -->
    <xsl:for-each select="$defs">
      <xsl:for-each select="def">
        <!--
        <xsl:value-of select="./@def-id" />
        <xsl:value-of select="deftext" />
        <xsl:value-of select="../gr/abbr" />
        <xsl:copy-of select="$CSEP" />

        -->
        <xsl:value-of select="ancestor::ar/k[1]" />
        <xsl:copy-of select="$CSEP" />
        <xsl:value-of select="./co[@type='uuid']" />
        <xsl:copy-of select="$CSEP" />
        <xsl:text>MEANS</xsl:text>
        <xsl:copy-of select="$RSEP" />
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
