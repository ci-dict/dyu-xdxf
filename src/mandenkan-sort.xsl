<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="xml" indent="yes" encoding="UTF-8" omit-xml-declaration="no"/>
  <!-- IdentityTransform -->
  <xsl:template match="/">
    <xsl:apply-templates select="@*|node()"/>
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()"/>
    </xsl:copy>
  </xsl:template>
  <xsl:template match="/xdxf/lexicon">
    <lexicon>
      <xsl:for-each select="ar">
        <xsl:sort select="k[1]" lang="en" data-type="text" order="ascending"/>
        <xsl:copy-of select="."/>
      </xsl:for-each>
    </lexicon>
  </xsl:template>
</xsl:stylesheet>
