<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="UTF-8" indent="true" omit-xml-declaration="yes" />

  <!-- IdentityTransform -->
  <xsl:template match="/xdxf/lexicon">
    <xsl:apply-templates select="@*|node()" />
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="/xdxf/lexicon" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/xdxf/lexicon">
    <xsl:variable name="defs" select="ar/def/def/def/def" />
    <!-- Data -->
    <lexicon>
      <xsl:for-each select="$defs">
        <xsl:sort select="../../../../k[1]" />
        <xsl:if test="categ = 'w100'">
          <ar>
            <id>
              <xsl:value-of select="./co[@type='uuid']" />
            </id>

            <freq>
              <xsl:choose>
                <xsl:when test="../@freq">
                  <xsl:value-of select="../@freq" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of select="../../../../def/@freq" />
                </xsl:otherwise>
              </xsl:choose>
            </freq>

            <value>
              <xsl:choose>
                <xsl:when test="../@freq">
                  <xsl:value-of
                    select="concat('group-', string(10 * floor(number(../@freq) div 20) + 10))" />
                </xsl:when>
                <xsl:otherwise>
                  <xsl:value-of
                    select="concat('group-', string(10 * floor(number(../../../../def/@freq) div 20) + 10))" />
                </xsl:otherwise>
              </xsl:choose>
            </value>

            <dyu>
              <xsl:value-of select="../../../../k[1]" />
            </dyu>
            <gr>
              <xsl:value-of select="../gr/abbr" />
            </gr>
            <fr>
              <xsl:value-of select="deftext" />
            </fr>
            <ex>
              <orig>
                <xsl:value-of select="ex[1]/ex_orig" />
              </orig>
              <trans>
                <xsl:value-of select="ex[1]/ex_tran" />
              </trans>
            </ex>
          </ar>
        </xsl:if>
      </xsl:for-each>
    </lexicon>
  </xsl:template>

</xsl:stylesheet>
