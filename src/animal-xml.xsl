<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" encoding="UTF-8" indent="true" omit-xml-declaration="yes" />

  <!-- IdentityTransform -->
  <xsl:template match="/xdxf/lexicon">
    <xsl:apply-templates select="@*|node()" />
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="/xdxf/lexicon" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/xdxf/lexicon">
    <xsl:variable name="defs" select="ar/def/def[@xml:lang='fr']/def/def" />
    <!-- Data -->
    <lexicon>
      <xsl:for-each select="$defs">
        <xsl:sort select="../../../../k[1]" />
        <xsl:if test="categ = 'animal'">
          <ar>
            <id>
              <xsl:value-of select="./co[@type='uuid']" />
            </id>
            <dyu>
              <xsl:value-of select="../../../../k[1]" />
            </dyu>
            <fr>
              <xsl:value-of select="deftext" />
            </fr>
            <ex>
              <orig>
                <xsl:value-of select="ex[1]/ex_orig[1]" />
              </orig>
              <trans>
                <xsl:value-of select="ex[1]/ex_tran[1]" />
              </trans>
            </ex>
            <usage>
              <xsl:value-of select="ex[1]/ex_orig[1]" />
            </usage>
          </ar>
        </xsl:if>
      </xsl:for-each>
    </lexicon>
  </xsl:template>

</xsl:stylesheet>
