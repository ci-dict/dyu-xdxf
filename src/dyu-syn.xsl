<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xsl:output method="text" encoding="UTF-8" />
  <xsl:param name="delim" select="','" />
  <xsl:param name="header" select="true()" />
  <xsl:param name="pipe" select="'|'" />
  <xsl:param name="colon" select="': '" />
  <xsl:param name="space" select="' '" />
  <xsl:param name="under" select="'_'" />
  <xsl:param name="TSEP" select="'&#09;'" />
  <xsl:param name="CSEP" select="','" />
  <xsl:param name="linkl" select="'[['" />
  <xsl:param name="linkr" select="']]'" />
  <xsl:param name="dir">/var/home/bkelly/Dropbox/lexique/pages/</xsl:param>

  <xsl:variable name="RSEP" select="'&#10;'" />  <!-- LF -->
  <!-- output tsv file for anki -->
  <!-- IdentityTransform -->
  <xsl:template match="/xdxf/lexicon">
    <xsl:apply-templates select="@*|node()" />
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="/xdxf/lexicon" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="/xdxf/lexicon">
    <!-- speech: the main elements -->
    <xsl:variable name="headword" select="ar" />
    <xsl:variable name="def" select="def/def/def/def" />
    <xsl:variable name="grs" select="$def/gr" />
    <!-- Data -->
    <xsl:for-each select="$headword">

      <xsl:variable name="output-doc" select="concat(k[1], '.md')" />
      <xsl:result-document href="{$dir}/{$output-doc}">

        <xsl:for-each select="def/def/def/def">
          <!-- the firt line will be the 'real' hw entry (not normalized)
      and the normalized (accents dropped verison) both as links 
          so all the spelling variantes and synonymes etc will have these common links -->
          <xsl:if test="position()= 1">
            <xsl:value-of select="'hw: '" />
            <xsl:value-of select="concat($linkl, ../../../../k[1], $linkr)" />
            <xsl:value-of select="$RSEP" />
            <xsl:value-of select="'syn: '" />
            <xsl:variable name="normalized"
              select="replace(normalize-unicode(../../../../k[1], 'NFD'), '\p{Mn}', '')" />
            <!-- <xsl:variable name="normalized" select="normalize-unicode(../../../k[1], 'NFD')"/> -->
            <xsl:if test="$normalized != ../../../../k[1]">
              <xsl:value-of select="concat($linkl, $normalized, $linkr)" />
            </xsl:if>
            <xsl:value-of select="$RSEP" />
          </xsl:if>

          <!-- only one definition needs all the spv variations -->
          <xsl:if test="position()= 1">
            <!-- spv spelling variations;  just need the actual spv as links-->
            <xsl:for-each select="../../../sr/kref">
              <xsl:variable name="normalized"
                select="replace(normalize-unicode(../../../../k[1], 'NFD'), '\p{Mn}', '')" />
              <!-- don't bother with the spv without accents.  we already got that with $normalized -->
              <xsl:if test="$normalized != ../../../../k[1]">
                <xsl:value-of select="./@type" />
                <xsl:value-of select="$colon" />
                <xsl:value-of select="concat($linkl, ., $linkr)" />
                <xsl:value-of select="$RSEP" />
              </xsl:if>
            </xsl:for-each>
          </xsl:if>

          <!-- synonymes and relations -->
          <xsl:for-each select="sr/kref">
            <xsl:value-of select="./@type" />
            <xsl:value-of select="$colon" />
            <xsl:value-of select="concat($linkl, ., $linkr)" />
            <xsl:value-of select="$RSEP" />
          </xsl:for-each>
        </xsl:for-each>

      </xsl:result-document>

    </xsl:for-each>
  </xsl:template>

</xsl:stylesheet>
