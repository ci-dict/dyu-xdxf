<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
xmlns:xsl  = "http://www.w3.org/1999/XSL/Transform"
xmlns:xs   = "http://www.w3.org/2001/XMLSchema">
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="header" select="true()" />
<xsl:param name="pipe" select="'|'" />
<xsl:param name="delim" select="','" />
<xsl:param name="equals" select="' = '" />
<xsl:param name="CSEP" select="'&#09;'" />
<xsl:variable name="RSEP" select="'&#10;'" />  <!-- LF -->

<!-- output tsv file for anki Examples-->

<!-- IdentityTransform -->
<xsl:template match="/xdxf/lexicon">
  <xsl:apply-templates select="@*|node()"/>
</xsl:template>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="/xdxf/lexicon"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="/xdxf/lexicon">
<!-- speech: the main elements -->
  <xsl:variable name="headwords" select="ar/k[@xml:lang='dyu'][1]" />
  <xsl:variable name="defs" select="../def/def[@xml:lang='fr']/def/def" />

  <!-- Header. -->
  <xsl:if test="$header">
    <!-- dyu -->
    <xsl:text>guid</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>hint1</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>dyu</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>french</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>tags</xsl:text>
    <xsl:copy-of select="$RSEP" />
  </xsl:if>

  <!-- Data -->
  <xsl:for-each select="$headwords">
    <xsl:for-each select="../def/def[@xml:lang='fr']/def/def">
      <xsl:for-each select="ex">
        <!-- t-uuid -->
        <xsl:if test="ex_orig != ''">
        <xsl:value-of select="current()/@ex-id" />
        <xsl:copy-of select="$CSEP" />

        <!-- hint1 (add french)-->
        <xsl:value-of select="ancestor::ar/k[1]" />
        <xsl:copy-of select="$equals" />
        <xsl:value-of select="../normalize-space(deftext)" />
        <xsl:copy-of select="$CSEP" />
        <xsl:value-of select="normalize-space(ex_orig)"/>
        <xsl:copy-of select="$CSEP" />
        <xsl:value-of select="normalize-space(ex_tran)"/>
        <xsl:copy-of select="$CSEP" />
        <xsl:text> </xsl:text>
        <xsl:copy-of select="$RSEP" />
      </xsl:if>
  </xsl:for-each>
  </xsl:for-each>
</xsl:for-each>
</xsl:template>

</xsl:stylesheet>

<!-- tags


  <xsl:value-of select="." separator="$delim"/>
  <xsl:for-each select="categ">

  <xsl:value-of select="." separator="$delim"/>

  </xsl:for-each>
  <xsl:if test="position() != last()">
  <xsl:value-of select="$delim" />
  </xsl:if>
-->
