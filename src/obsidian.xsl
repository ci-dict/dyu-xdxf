<?xml version="1.0"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="text" encoding="UTF-8" />
  <xsl:param name="asciidoctor" select="'a| '" />
  <xsl:param name="asterisk" select="'* '" />
  <xsl:param name="delim" select="','" />
  <xsl:param name="hash" select="'#'" />
  <xsl:param name="h1" select="'# '" />
  <xsl:param name="card" select="'#card'" />
  <xsl:param name="colon" select="':'" />
  <xsl:param name="doublecolon" select="'::'" />
  <xsl:param name="level1" select="'* '" />
  <xsl:param name="level2" select="'  * '" />
  <xsl:param name="level3" select="'    * '" />
  <xsl:param name="level4" select="'      * '" />
  <xsl:param name="level5" select="'        * '" />
  <xsl:param name="level6" select="'          * '" />
  <xsl:param name="space3" select="'    '" />
  <xsl:param name="space4" select="'      '" />
  <xsl:param name="space5" select="'        '" />
  <xsl:param name="space6" select="'          '" />
  <xsl:param name="newline" select="'&#10;'" />
  <xsl:param name="linkl" select="'[['" />
  <xsl:param name="linkr" select="']]'" />
  <xsl:param name="parl" select="'('" />
  <xsl:param name="parr" select="')'" />
  <xsl:param name="plus" select="' +'" />
  <xsl:param name="period" select="'. '" />
  <xsl:param name="right_bottom" select="'>.>| '" />
  <xsl:param name="separator" select="'|'" />
  <xsl:param name="tdef" select="'|==='" />
  <xsl:strip-space elements="*" />
  <xsl:key name="initialChar" match="xdxf/lexicon/ar"
    use="upper-case(replace(normalize-unicode(substring(k[1],1,1),'NFD'), '\p{Mn}', ''))" />
  <!-- IdentityTransform -->
  <xsl:template match="/xdxf/lexicon">
    <xsl:apply-templates select="@*|node()" />
  </xsl:template>
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="/xdxf/lexicon" />
    </xsl:copy>
  </xsl:template>
  <xsl:template match="/xdxf/lexicon">
    <xsl:variable name="ars" select="ar" />
    <xsl:variable name="defs" select="/def/def" />
    <xsl:variable name="grs" select="$defs/gr" />
    <!-- header -->
    <!-- <xsl:text>{{renderer :toc_ueqmhoxbk}}</xsl:text> -->
    <!-- <xsl:text>{{embed [[Index]]}}</xsl:text> -->
    <xsl:for-each
      select="//ar[generate-id(.)=generate-id(key('initialChar', upper-case(replace(normalize-unicode(substring(k[1],1,1),'NFD'), '\p{Mn}', '')))[1])]">
      <!-- sort here -->
      <xsl:sort select="k[1]" lang="en" data-type="text" order="ascending" />
      <xsl:variable name="myChar"
        select="upper-case(replace(normalize-unicode(substring(k[1],1,1),'NFD'), '\p{Mn}', ''))" />
      <!-- alphabet heading -->
      <xsl:value-of
        select="concat($newline, $level1, $h1, $linkl, string($myChar), $linkr, $newline)" />
      <!-- iterate over all the unique initial letter values -->
      <!--   headword in dyu (k) here    -->
      <xsl:for-each select="key('initialChar',$myChar)">
        <xsl:sort select="k[1]" />
        <xsl:value-of select="concat($newline, $level2, $linkl, k[1], $linkr, ' ')" />
        <!-- now for the defs -->
        <xsl:variable name="countSPV" select="count(def/sr/kref[@type='spv'])" />
        <xsl:if test="$countSPV &gt; 0 and not(normalize-space(def/sr/kref[@type='spv'][1])='')">
          <xsl:for-each select="def/sr/kref[@type='spv']">
            <xsl:value-of select="concat($hash, .,' ')" />
          </xsl:for-each>
        </xsl:if>
        <xsl:value-of select="$newline" />
        <xsl:for-each select="def/def[@xml:lang='fr']/def/gr">
          <xsl:if test="../gr/abbr != ''">
            <!-- part of speech here -->
            <!-- <xsl:value-of select="concat($level2, '(', ../gr/abbr,') ', $linkl, deftext,
            $linkr)"/> -->
            <xsl:value-of select="concat($level3, '(', ../gr/abbr, ')', $newline)" />
          </xsl:if>
          <xsl:variable name="countDEF" select="count(../def/deftext)" />
          <xsl:for-each select="../def">
            <xsl:if test="$countDEF &gt; 1">
              <xsl:if test="position() &gt; 1">
                <xsl:value-of select="concat($level4, position(), '. ', deftext, $newline)" />
              </xsl:if>
              <xsl:if test="position() = 1">
                <xsl:value-of select="concat($level4, position(), '. ', deftext, $newline)" />
              </xsl:if>
            </xsl:if>
            <xsl:if test="$countDEF &lt; 2">
              <xsl:value-of select="concat($level4, deftext, $newline)" />
            </xsl:if>
            <xsl:for-each select="ex">
              <xsl:if test="ex_orig != ''">
                <xsl:value-of select="(concat($level5, ex_orig, $newline))" />
                <xsl:value-of select="(concat($level6, ex_tran, $newline))" />
              </xsl:if>
            </xsl:for-each>
            <xsl:variable name="countSYN" select="count(sr/kref[@type='syn' or @type='rel'])" />
            <xsl:if test="$countSYN &gt; 0">
              <xsl:value-of select="concat($level5, 'cf. ' )" />
              <xsl:for-each select="sr/kref[@type='syn' or @type='rel']">
                <xsl:value-of select="concat($hash, .,' ')" />
              </xsl:for-each>
              <xsl:value-of select="$newline" />
            </xsl:if>
            <xsl:if test="categ != ''">
              <xsl:value-of select="$space5" />
              <xsl:for-each select="categ">
                <xsl:value-of select="concat($hash, .,' ')" />
              </xsl:for-each>
              <xsl:value-of select="$newline" />
            </xsl:if>
          </xsl:for-each>
        </xsl:for-each>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
