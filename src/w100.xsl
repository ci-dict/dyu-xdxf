<?xml version="1.0"?>
<xsl:stylesheet version="2.0"
  xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
  xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xsl:output method="text" encoding="UTF-8" />
  <xsl:param name="header" select="true()" />
  <xsl:param name="CSEP" select="'&#09; '" />
  <xsl:variable name="RSEP" select="'&#10;'" />  <!-- LF -->

  <!-- IdentityTransform -->
  <xsl:template match="/xdxf/lexicon">
    <xsl:apply-templates select="@*|node()" />
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="/xdxf/lexicon" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/xdxf/lexicon">
    <!-- speech: the main elements -->
    <xsl:variable name="defs" select="ar/def/def/def/def" />

    <!-- Header. -->
    <xsl:if test="$header">
      <xsl:text>Fréquence</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>Dioula</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>Gr.</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>Français</xsl:text>
      <xsl:copy-of select="$CSEP" />
      <xsl:text>Usage</xsl:text>
      <xsl:copy-of select="$RSEP" />
    </xsl:if>

    <!-- Data -->
    <xsl:for-each select="$defs">
      <xsl:sort select="../../../../k[1]" />
      <xsl:if test="categ = 'w100'">
        <xsl:choose>
          <xsl:when test="../@freq">
            <xsl:value-of select="../@freq" />
          </xsl:when>
          <xsl:otherwise>
            <xsl:value-of select="../../../../def/@freq" />
          </xsl:otherwise>
        </xsl:choose>

        <xsl:copy-of select="$CSEP" />
        <xsl:value-of select="../../../../k[1]" />
        <xsl:copy-of select="$CSEP" />
        <xsl:copy-of select="../gr/abbr" />
        <xsl:copy-of select="$CSEP" />
        <xsl:copy-of select="deftext" />
        <xsl:copy-of select="$CSEP" />
        <xsl:value-of select="ex[1]/ex_orig" />
        <xsl:copy-of select="$RSEP" />
      </xsl:if>
    </xsl:for-each>

  </xsl:template>

</xsl:stylesheet>
