<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
xmlns:xsl  = "http://www.w3.org/1999/XSL/Transform" 
xmlns:xs   = "http://www.w3.org/2001/XMLSchema">
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="header" select="true()" />
<xsl:param name="pipe" select="':'" />
<xsl:param name="delim" select="','" />
<xsl:param name="equals" select="' = '" />
<xsl:param name="CSEP" select="'&#09;'" />
<xsl:variable name="RSEP" select="'&#10;'" />  <!-- LF -->

<!-- IdentityTransform -->
<xsl:template match="/xdxf/lexicon">
  <xsl:apply-templates select="@*|node()"/>
</xsl:template>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="/xdxf/lexicon"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="/xdxf/lexicon">
<!-- speech: the main elements -->
  <xsl:variable name="headwords" select="ar" />
  <xsl:variable name="defs" select="ar/def/def/def" />

  <!-- Header. -->
  <xsl:if test="$header">
    <!-- dyu -->
    <xsl:text>uuid:ID</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>text</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>lang</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>type</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>:LABEL</xsl:text>
    <xsl:copy-of select="$RSEP" />
  </xsl:if>

  <!-- Data -->
  <xsl:for-each select="$defs">
    <xsl:for-each select="def">
      <xsl:for-each select="ex">
        <!-- t-uuid -->
        <xsl:if test="ex_orig != ''">
          <xsl:value-of select="./@ex-id" />
          <xsl:copy-of select="$CSEP" />
          <xsl:value-of select="normalize-space(ex_orig)"/>
          <xsl:copy-of select="$CSEP" />
          <xsl:text>dyu</xsl:text>
          <xsl:copy-of select="$CSEP" />
          <xsl:value-of select="./@type" />
          <xsl:copy-of select="$CSEP" />
          <xsl:text>dyuEx</xsl:text>
          <xsl:copy-of select="$RSEP" />
          <xsl:value-of select="translate(./@ex-id, '-', '_')"/>
          <xsl:copy-of select="$CSEP" />
          <xsl:value-of select="normalize-space(ex_tran)"/>
          <xsl:copy-of select="$CSEP" />
          <xsl:text>fr</xsl:text>
          <xsl:copy-of select="$CSEP" />
          <xsl:value-of select="./@type" />
          <xsl:copy-of select="$CSEP" />
          <xsl:text>frEx</xsl:text>
          <xsl:copy-of select="$RSEP" />
        </xsl:if>
      </xsl:for-each>
    </xsl:for-each>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>

