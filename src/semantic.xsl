<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">
  <xsl:output method="html" encoding="UTF-8" doctype-public="" indent="yes"/>
  <xsl:param name="newline" select="'&#10;'"/>
  <xsl:strip-space elements="*"/>

  <xsl:key name="initialChar" match="xdxf/lexicon/ar" use="upper-case(replace(normalize-unicode(substring(k[1],1,1),'NFD'), '\p{Mn}', ''))"/>

  <!-- IdentityTransform -->
  <xsl:template match="/xdxf/lexicon">
    <xsl:apply-templates select="@*|node()"/>
  </xsl:template>

  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="/xdxf/lexicon"/>
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/xdxf/lexicon">
    <xsl:variable name="ars" select="ar"/>
    <xsl:variable name="defs" select="/def/def"/>
    <xsl:variable name="grs" select="$defs/gr"/>

    <xsl:for-each select="//ar[generate-id(.)=generate-id(key('initialChar', upper-case(replace(normalize-unicode(substring(k[1],1,1),'NFD'), '\p{Mn}', '')))[1])]">
      <!-- sort here -->
      <xsl:sort select="k[1]" lang="en" data-type="text" order="ascending"/>

      <xsl:variable name="myChar" select="upper-case(replace(normalize-unicode(substring(k[1],1,1),'NFD'), '\p{Mn}', ''))"/>

      <!-- output the level1 header  and start asciidoc table-->
      <xsl:value-of select="$newline"/>
      <section class="letter">
        <h3>
          <xsl:attribute name="id">
            <xsl:value-of select="translate(concat('_', string($myChar)), 'ABCDE&#x190;FGHIJKLMN&#x19D;O&#x186;PQRSTUVWXYZ', 'abcde&#x25B;fghijklmn&#x272;o&#x254;pqrstuvwxyz')"/>
          </xsl:attribute>
          <xsl:value-of select="string($myChar)"/>
        </h3>

        <xsl:value-of select="$newline"/>
        <!-- iterate over all the unique initial letter values -->
        <!--   The defs go here    -->
        <xsl:for-each select="key('initialChar',$myChar)">
          <xsl:sort select="k[1]"/>
          <article class="dl">
            <xsl:variable name="normalizedContent" select="normalize-unicode(k[1])"/>
            <h4 id="{$normalizedContent}" xml:lang="dyu" class="k">
              <xsl:value-of select="k" separator=" | "/>
            </h4>

            <xsl:for-each select="def/def[@xml:lang='fr']">
              <section class='definition'>
                <xsl:variable name="countSPV" select="count(../sr/kref[@type='spv'])"/>
                <!-- <xsl:if test="$countSPV &gt; 0 and not(normalize-space(sr/kref[@type='spv'][1])='')"> -->
                <xsl:if test="$countSPV &gt; 0">
                  <div xml:lang="dyu" class="spv"> (<xsl:value-of select="../sr/kref[@type='spv']" separator=" | "/>) </div>
                  <xsl:value-of select="$newline"/>
                </xsl:if>

                <xsl:if test="co != ''">
                  <xsl:text/>
                  <xsl:value-of select="co"/>
                  <xsl:value-of select="$newline"/>
                </xsl:if>

                <xsl:for-each select="def">
                  <ul class="pos">
                    <xsl:value-of select="$newline"/>
                    <xsl:if test="gr/abbr != ''">
                      <xsl:value-of select="$newline"/>
                      <li class="gr">
                        <xsl:value-of select="gr/abbr"/>
                      </li>
                      <xsl:value-of select="$newline"/>
                    </xsl:if>

                    <xsl:if test="co != ''">
                      <div class="co">
                        <xsl:value-of select="co"/>
                      </div>
                      <xsl:value-of select="$newline"/>
                    </xsl:if>

                    <!--   Only number defs when more than one    -->
                    <xsl:variable name="onedef" select="count(def/deftext)"/>

                    <xsl:if test="$onedef &gt; 1">
                      <ol class="def">
                        <xsl:for-each select="def">
                          <xsl:value-of select=" $newline"/>
                          <li>
                            <xsl:value-of select="deftext"/>
                            <xsl:value-of select=" $newline"/>
                            <dl class="examples">
                              <xsl:for-each select=" ex">
                                <xsl:if test=" ex_orig !=''">
                                  <dt class="orig">
                                    <xsl:value-of select="ex_orig"/>
                                  </dt>
                                  <dd xml:lang="dyu" class="trans">
                                    <xsl:value-of select="ex_tran"/>
                                  </dd>
                                  <xsl:value-of select=" $newline"/>
                                </xsl:if>
                              </xsl:for-each>
                            </dl>
                            <xsl:variable name=" countSYN" select=" count(sr/ kref[@type='syn' ])"/>
                            <xsl:if test=" $countSYN &gt; 0">
                              <aside>
                                <xsl:text>cf. </xsl:text>
                                <xsl:value-of select=" sr/ kref[@type='syn' ]" separator=" | "/>
                              </aside>
                              <xsl:value-of select=" $newline"/>
                            </xsl:if>
                          </li>

                        </xsl:for-each>
                      </ol>
                    </xsl:if>

                    <!--   Unordered list when only one def     -->
                    <xsl:if test="$onedef = 1">
                      <ul class="def">
                        <xsl:for-each select="def">
                          <xsl:value-of select=" $newline"/>
                          <li>
                            <xsl:value-of select="deftext"/>
                            <xsl:value-of select=" $newline"/>
                            <dl class="examples">
                              <xsl:for-each select=" ex">
                                <xsl:if test=" ex_orig !=''">
                                  <dt xml:lang="dyu" class="orig">
                                    <xsl:value-of select="ex_orig"/>
                                  </dt>
                                  <dd class="trans">
                                    <xsl:value-of select="ex_tran"/>
                                  </dd>
                                  <xsl:value-of select=" $newline"/>
                                </xsl:if>
                              </xsl:for-each>
                            </dl>
                            <xsl:variable name=" countSYN" select=" count(sr/ kref[@type='syn' ])"/>
                            <xsl:if test=" $countSYN &gt; 0">
                              <aside>
                                <xsl:text>cf. </xsl:text>
                                <xsl:value-of select=" sr/ kref[@type='syn' ]" separator=" | "/>
                              </aside>
                              <xsl:value-of select=" $newline"/>
                            </xsl:if>
                          </li>

                        </xsl:for-each>
                      </ul>
                    </xsl:if>
                  </ul>
                </xsl:for-each>
              </section>
            </xsl:for-each>
            <xsl:value-of select=" $newline"/>
          </article>
        </xsl:for-each>

      </section>
    </xsl:for-each>
  </xsl:template>
</xsl:stylesheet>
