<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
xmlns:xsl  = "http://www.w3.org/1999/XSL/Transform" 
xmlns:xs   = "http://www.w3.org/2001/XMLSchema">
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="header" select="true()" />
<xsl:param name="pipe" select="' | '" />
<xsl:param name="square_l" select="'[ '" />
<xsl:param name="square_r" select="' ]'" />
<xsl:param name="colon" select="' : '" />
<xsl:param name="delim" select="','" />
<xsl:param name="CSEP" select="'&#09;'" />
<xsl:variable name="RSEP" select="'&#10;'" />  <!-- LF -->

<!-- <xsl:variable name="maxExamples" select="max($exampleCount/count/@count) cast as xs:integer" />-->
<xsl:variable name="maxExamples" select="18 cast as xs:integer" />
<!-- output tsv file for anki -->

<!-- IdentityTransform -->
<xsl:template match="/xdxf/lexicon">
  <xsl:apply-templates select="@*|node()"/>
</xsl:template>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="/xdxf/lexicon"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="/xdxf/lexicon">
<!-- speech: the main elements -->
  <xsl:variable name="headwords" select="ar/def" />
  <xsl:variable name="defs" select="$headwords/def/def/def" />
  <xsl:variable name="grs" select="$defs/gr" />

  <!-- Header. -->
  <xsl:if test="$header">
    <!-- dyu -->
    <xsl:text>text</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>orth</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>freq</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>lexicon</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>LABEL</xsl:text>
    <xsl:copy-of select="$RSEP" />

  </xsl:if>

  <!-- Data -->
  <xsl:for-each select="$headwords">
    <!-- t-uuid -->
    <!-- dyu -->
    <xsl:value-of select="../k[1]" />
      <xsl:copy-of select="$CSEP" />

    <!-- orth -->
      <xsl:copy-of select="$square_l" />
      <xsl:value-of select="../k[1]" />

    <xsl:for-each select="sr/kref[@type='spv']">
      <xsl:if test="current() != ''">
      <xsl:copy-of select="$delim" />
      <xsl:value-of select="."/>
    </xsl:if>
    </xsl:for-each>

      <xsl:copy-of select="$square_r" />
      <xsl:copy-of select="$CSEP" />

    <!-- freq -->
      <xsl:value-of select="./@freq" />
      <xsl:copy-of select="$CSEP" />

      <xsl:text>true</xsl:text>
      <xsl:copy-of select="$CSEP" />

    <xsl:text>Headword</xsl:text>
    <xsl:copy-of select="$RSEP" />

    <!--
    <xsl:for-each select="../../sr/kref[@type='spv']">
      <xsl:if test="current() != ''">
      <xsl:value-of select="concat($defid, '-', .)"/>
      <xsl:copy-of select="$CSEP" />
      <xsl:value-of select="."/>
      <xsl:copy-of select="$CSEP" />
      <xsl:copy-of select="$CSEP" />

      <xsl:text>Orth</xsl:text>
      <xsl:copy-of select="$RSEP" />
    </xsl:if>
    </xsl:for-each>
    -->

  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>

