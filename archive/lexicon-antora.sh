#!/bin/bash
set -o nounset
set -o pipefail
set -e

for x in jq yq csvclean brainbrew xslt3; do
	type -P $x >/dev/null 2>&1 || {
		echo >&2 "${x} not installed.  Aborting."
		exit 0
	}
done

#[[ -z ""$1"" ]] && { echo "You need to specify a file from nolinks"; exit 1; }

function validate {
	echo Validating "$1" filetype: "${1#*.}"
	if [ "${1##*.}" == "tsv" ]; then
		csvclean -n -v -t "$1"
		awk 'BEGIN{FS="\t"} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' "$1"
	else
		csvclean -n -v "$1"
		#		awk 'BEGIN{FS=","} !n{n=NF}n!=NF{rc=1;print "Error line " NR " " NF " fields"; exit rc} END{print NF " fields" }' "$1"
	fi
	wc -l "$1"
	#delete missing field 1
	#  sed -i /^,/d "$1"
	#remove any defs with discontined verbs for now
	#  sed -i /[...]/d "$1"
}

# validate mandenkan/lexicon.csv
# validate mandenkan/lexicon.tsv

function count {
	# cat "$1" | awk -v file="$1" '{n += gsub(/\<ar\>/, "")} END {print n " entries in " file}'
	echo "$(sed -n '/<ar>/p' $1 | wc -l)" entries in "$1"
}

function yq {
	/home/bkelly/go/bin/yq "$@"
}
export -f yq

#set -o errexit
file=lexicon
project=mandenkan
dict=./$project/dict.xdxf

for x in tsv tmp xml xsd xsl adoc json yml md; do export "${x}=${file}.$x"; done
#yml=./dict.yml

echo $xml
echo $yml
echo $json
[[ -f "$project"/"$xml" ]] && count $project/"$xml"

export DATE=$(date -I)
yq -i '.xdxf.meta_info.last_edited_date = strenv(DATE)' "$yml"

yq -o=x - <"$yml" >"$dict"
yq -o=j - <"$yml" >"$project/$json"

#tidy -q -m -xml -indent $dict
#sed -i '1 i <!DOCTYPE xdxf SYSTEM "xdxf_strict.dtd">' $dict
#sed -i '1 i <?xml version="1.0" encoding="UTF-8" ?>' $dict
xmllint --noout --dtdvalid $project/xdxf_strict.dtd $dict

ln -f $dict ./$project/$xml

count $project/$xml

for x in lexicon phrases animal.fr animal.dyu; do
	npx xslt3 -xsl:$x.xsl -s:$dict >$project/$x.tsv
	sleep 1
done

csvtool -t TAB -u COMMA cat $project/lexicon.tsv >$project/lexicon.csv

npx xslt3 -xsl:w100.xsl -s:$dict | {
	head -n 1
	sort -t$'\t' -k1 -h
} >$project/w100.tsv

npx xslt3 -xsl:$file-adoc.xsl -s:"$project/$xml" >vocab-dyu.adoc
#
#as done by collector now
cp -v vocab-dyu.adoc /home/bkelly/dev/svelte/dyu-lex/src/routes/dyu/_include

# for the time being...
npx xslt3 -xsl:dyu-refs.xsl -s:"$project/$xml" | sort -u -t, -k1,1 >$project/dyu-refs.csv
npx xslt3 -xsl:dyu-links.xsl -s:"$project/$xml" | sort -u -t, -k1,1 >$project/dyu-links.csv
npx xslt3 -xsl:dyu-uri.xsl -s:"$project/$xml" | sort -u -t, -k1,1 >$project/dyu-uri.csv
# npx xslt3 -xsl:dyu-syn.xsl -s:"$project/$xml" | sort -u -t, -k1,1 >$project/dyu-syn.csv
# validate $project/dyu-syn.csv

#logseq
npx xslt3 -xsl:logseq.xsl -s:"$project/$xml" >~/Dropbox/lexique/pages/contents.md
npx xslt3 -xsl:obsidian.xsl -s:"$project/$xml" >~/Documents/Jula/lexique/lexique/Contents.md

# neo4j

for x in hw def ex r_means r_examples r_usage hw_keys; do
	npx xslt3 -xsl:neo_$x.xsl -s:"$project/$xml" >$project/neo_$x.tsv
done
sort -u $project/neo_hw_keys.tsv -o $project/neo_hw_keys.tsv

#lets check these before copying
for x in "$project"/*tsv; do validate "$x"; done
for x in "$project"/*csv; do validate "$x"; done

targetdir="$HOME/neo4j/import"
[[ -d "$HOME/neo4j/import" ]] && sudo cp -v "./$project"/neo_*.tsv "$targetdir/"
cp -v $project/animal.fr.tsv /var/home/bkelly/dev/web-docs/content/fr/modules/ROOT/partials/animal.tsv
cp -v $project/animal.dyu.tsv /var/home/bkelly/dev/web-docs/content/dyu/modules/ROOT/partials/animal.tsv
cp -v $project/animal.fr.tsv /var/home/bkelly/dev/svelte/coastsystems.net/src/routes/fr/_include/
#note the name change
cp -v $project/animal.dyu.tsv /var/home/bkelly/dev/jula/slides-dyu/animal.dyu.tsv
cp -v $project/w100.tsv /var/home/bkelly/dev/web-docs/content/fr/modules/ROOT/partials/
cp -v $project/w100.tsv /var/home/bkelly/dev/web-docs/docs/fr/modules/ROOT/partials/
cp -v $project/w100.tsv /var/home/bkelly/dev/web-docs/content/en/modules/ROOT/partials/
cp -v $project/w100.tsv /var/home/bkelly/dev/web-docs/docs/en/modules/ROOT/partials/
cp -v $project/w100.tsv /var/home/bkelly/dev/svelte/coastsystems.net/src/routes/en/_include/
cp -v $project/w100.tsv /var/home/bkelly/dev/svelte/coastsystems.net/src/routes/fr/_include/

cp -v $project/lexicon.tsv /home/bkelly/Anki/brainbrew/Jula/src/data/update.tsv
cp -v $project/phrases.tsv /home/bkelly/Anki/brainbrew/Examples/src/data/Phrases.tsv
cp -v $project/phrases.tsv /home/bkelly/dev/jula/slides-dyu/
git diff-index --quiet HEAD -- || git commit -a -m "updated on: $(date) by $0"
git push origin main

git -C /home/bkelly/dev/web-docs submodule update --remote
git -C /home/bkelly/dev/web-docs commit -a -m "updated on: $(date) by $0"

echo "update slides"
update="/home/bkelly/dev/jula/slides-dyu/"
git -C $update diff-index --quiet HEAD -- || git -C $update commit -a -m "updated on: $(date) by $0"
git -C $update push origin main

exit

echo "Update brainbrew"
update=/home/bkelly/Anki/brainbrew/Jula/build/Jula/
brainbrew=/home/bkelly/Anki/brainbrew/Jula/
pushd $brainbrew || exit
./upd-headwords-step1.sh french && ./upd-step2.sh
popd || exit
git -C $update diff-index --quiet HEAD -- || git -C $update commit -a -m "updated on: $(date) by $0"
git -C $update push origin main

update=/home/bkelly/Anki/brainbrew/Examples/build/Examples/
brainbrew=/home/bkelly/Anki/brainbrew/Examples/
git -C $update pull origin main
pushd $brainbrew && brainbrew run recipes/source_to_anki.yaml && popd
git -C $update diff-index --quiet HEAD -- || git -C $update commit -a -m "updated on: $(date) by $0"
git -C $update push origin main
