#!/bin/bash
set -o errexit
set -o nounset
set -o pipefail
project=mandenkan

pushd $project
#awk 'BEGIN {FS = "\t";OFS = "\t" ; print "text:ID	lang:string	type:string	:LABEL" }
#{print $1, "dyu", "prv", "Proverb"}' proverbs.tsv > neo_prv_dyu.tsv
#
#awk 'BEGIN {FS = "\t";OFS = "\t" ; print "text:ID	lang:string	type:string	:LABEL" }
#{print $2, "fr", "prv", "Proverb"}' proverbs.tsv > neo_prv_fr.tsv
#
#awk 'BEGIN {FS = "\t";OFS = "\t" ; print ":START_ID	lang:string	:END_ID	:TYPE" }
#{print $1, "fr", $2, "MEANS"}' proverbs.tsv > neo_r_prv_fr.tsv
#
#unused for now.  Reverse relation from french to jula proverb
#awk 'BEGIN {FS = "\t";OFS = "\t" ; print ":START_ID	lang:string	:END_ID	:TYPE" }
#{print $2, "dyu", $1, "MEANS"}' proverbs.tsv > neo_r_prv_dyu.tsv
popd
# the following provides the relationships for the multiple spelling
# variations to the proverbs containing words in dictionary.
FILE1="$project/neo_hw_keys.tsv"
FILE2="$project/proverbs.tsv"

# question stackexchange
#The header needs to go first otherwise it will get sorted
#sort by first two fields unique otheriwse will get double relations when word is twice in expression
printf "START_ID\tEND_ID\tTYPE\n" > $project/neo_r_hw_prv.tsv
awk '
  BEGIN {FS = OFS = "\t";}
  NR == FNR {kw[$1] = $2; next}
  {
    n = split(tolower($1), words, /[[:blank:]]|\.|,/)
    for (i = 1; i <= n; i++) {
      if (words[i] in kw && length(words[i]) > 2) print kw[words[i]], $1, "PROVERB"
    }
  }
' $FILE1 $FILE2 | sort -t $'\t' -u -k1,1 -k2  >> $project/neo_r_hw_prv.tsv

#$project/neo_r_hw_prv.tsv

FILE2="$project/phrases.tsv"
awk '
  BEGIN {FS = OFS = "\t";}
  NR == FNR {kw[$1] = $2; next}
  {
    n = split(tolower($3), words, /[[:blank:]]+/)
    for (i = 1; i <= n; i++) {
      if (words[i] in kw && length(words[i]) > 3) print kw[words[i]], $3, "PHRASE"
    }
  }
' $FILE1 $FILE2 | sort -t $'\t' -u -k1,1 -k2  > $project/neo_r_hw_phrases.tsv
 
FILE2="$project/blessings.tsv"
awk '
  BEGIN {FS = OFS = "\t";}
  NR == FNR {kw[$1] = $2; next}
  {
    n = split(tolower($1), words, /[[:blank:]]+/)
    for (i = 1; i <= n; i++) {
      if (words[i] in kw && length(words[i]) > 3) print kw[words[i]], $1, "BLESSING"
    }
  }
' $FILE1 $FILE2 | sort -t $'\t' -u -k1,1 -k2  > $project/neo_r_hw_blessings.tsv
