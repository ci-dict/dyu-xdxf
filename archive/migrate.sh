#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

project=project
file=test.yaml

#[ -f ${HEADWORDS} ] || { echo "${HEADWORDS} not found.  Exiting..."; exit 1; }

for x in tmp xml xsd yaml adoc json; do export "${x}=${project}.$x"; done

for x in sed ; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

sed -i 's/headword:/ar:/g' $file 
sed -i 's/dyu:/k:/g' $file 
sed -i 's/alt:/spv:/g' $file 
sed -i 's/cf:/syn:/g' $file 
sed -i 's/type:/gr:/g' $file 
sed -i 's/example:/exm:/g' $file 
sed -i 's/source:/ex_origin:/g' $file 
sed -i 's/target:/ex_tran:/g' $file 
