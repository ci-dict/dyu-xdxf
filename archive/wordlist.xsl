<?xml version="1.0"?>
<xsl:stylesheet version="2.0" 
xmlns:xsl  = "http://www.w3.org/1999/XSL/Transform" 
xmlns:xs   = "http://www.w3.org/2001/XMLSchema">
<xsl:output method="text" encoding="UTF-8" />
<xsl:param name="header" select="true()" />
<xsl:param name="pipe" select="' | '" />
<xsl:param name="delim" select="','" />
<xsl:param name="CSEP" select="'&#09;'" />
<xsl:variable name="RSEP" select="'&#10;'" />  <!-- LF -->

<!-- <xsl:variable name="maxExamples" select="max($exampleCount/count/@count) cast as xs:integer" />-->
<xsl:variable name="maxExamples" select="18 cast as xs:integer" />
<!-- output tsv file for anki -->

<!-- IdentityTransform -->
<xsl:template match="/xdxf/lexicon">
  <xsl:apply-templates select="@*|node()"/>
</xsl:template>

<xsl:template match="@*|node()">
  <xsl:copy>
    <xsl:apply-templates select="/xdxf/lexicon"/>
  </xsl:copy>
</xsl:template>

<xsl:template match="/xdxf/lexicon">
<!-- speech: the main elements -->
  <xsl:variable name="headwords" select="ar/k[@xml:lang='dyu'][1]" />
  <xsl:variable name="defs" select="../def/def[@xml:lang='fr']/def" />
  <xsl:variable name="grs" select="$defs/gr" />

  <!-- Header. -->
  <xsl:if test="$header">
    <!-- dyu -->
    <xsl:text>guid</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>dyu</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>source</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>speech</xsl:text>
    <!-- gloss -->
    <xsl:copy-of select="$CSEP" />
    <xsl:text>french</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>hint1</xsl:text>
    <xsl:copy-of select="$CSEP" />

    <!-- alt no longer used -->
    <xsl:for-each select="(1 to 4)">
      <xsl:text>alt</xsl:text>
      <xsl:value-of select="." />
      <xsl:copy-of select="$CSEP" />
    </xsl:for-each>

    <!-- emp -->
    <xsl:text>emp</xsl:text>
    <xsl:copy-of select="$CSEP" />

    <xsl:for-each select="(1 to 4)">
      <xsl:text>cf</xsl:text>
      <xsl:value-of select="." />
      <xsl:copy-of select="$CSEP" />
    </xsl:for-each>

    <!-- anki note  -->
    <xsl:for-each select="(1 to 4)">
      <xsl:text>note</xsl:text>
      <xsl:value-of select="." />
      <xsl:copy-of select="$CSEP" />
    </xsl:for-each>

    <!-- anki text2,3,4 (not used here) -->
    <xsl:for-each select="(2 to 4)">
      <xsl:text>text</xsl:text>
      <xsl:value-of select="." />
      <xsl:copy-of select="$CSEP" />
    </xsl:for-each>

    <!-- anki examples (not used here) -->
    <xsl:for-each select="(1 to 4)">
      <xsl:text>example</xsl:text>
      <xsl:value-of select="." />
      <xsl:copy-of select="$CSEP" />
    </xsl:for-each>

    <!-- source1,target1..sourceN,targetN -->
    <xsl:for-each select="(1 to $maxExamples)">
      <xsl:text>source</xsl:text>
      <xsl:value-of select="." />
      <xsl:copy-of select="$CSEP" />
      <xsl:text>target</xsl:text>
      <xsl:value-of select="." />
      <xsl:copy-of select="$CSEP" />
    </xsl:for-each>
    <xsl:text>english</xsl:text>
    <xsl:copy-of select="$CSEP" />
    <xsl:text>tags</xsl:text>
    <xsl:copy-of select="$RSEP" />

  </xsl:if>

    <!-- Data -->
  <xsl:for-each select="$headwords">
    <xsl:for-each select="../def/def[@xml:lang='fr']/def">
      <!-- t-uuid -->
      <xsl:value-of select="@def-id" />
      <xsl:copy-of select="$CSEP" />

      <!-- dyu -->
      <xsl:value-of select="../../../k[1]" />
      <xsl:copy-of select="$CSEP" />

      <!-- no source can we use for something else?-->

      <xsl:choose>
        <xsl:when test="./@freq"><xsl:value-of select="./@freq" /></xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="../../../def/@freq" />
        </xsl:otherwise>
      </xsl:choose>

      <xsl:copy-of select="$CSEP" />

      <!-- speech -->
      <xsl:value-of select="gr/abbr" />
      <xsl:copy-of select="$CSEP" />

      <!-- gloss/french consolidate the multiple french defs-->
      <xsl:for-each select="def">
        <xsl:value-of select="deftext" />
        <xsl:if test="position() != last()">
          <xsl:value-of select="$pipe" />
        </xsl:if>
      </xsl:for-each>
      <xsl:copy-of select="$CSEP" />

      <!-- hint1  is emprunt-->
      <xsl:copy-of select="../../../sr/kref[@type='par']" />
      <xsl:copy-of select="$CSEP" />

      <!-- Alt-->
      <xsl:variable name="countALT" select="count(../../sr/kref[@type='spv'])"/>
      <xsl:for-each select="../../sr/kref[@type='spv']">
        <xsl:value-of select="."/>
        <xsl:copy-of select="$CSEP" />
      </xsl:for-each>

      <xsl:for-each select="(1 to 4 - $countALT)">
        <xsl:copy-of select="$CSEP" />
      </xsl:for-each>

      <!--emp -->
      <xsl:copy-of select="../../sr/kref[@type='par']" />
      <xsl:copy-of select="$CSEP" />

      <!--cf -->
      <xsl:variable name="countCF" select="count(def/sr/kref[@type='syn'])" />
      <xsl:value-of select="def/sr/kref[@type='syn']" separator="&#09;" />

      <xsl:if test="$countCF &gt; 0" >
        <xsl:copy-of select="$CSEP" />
      </xsl:if>

      <xsl:for-each select="(1 to 4 - $countCF)">
        <xsl:copy-of select="$CSEP" />
      </xsl:for-each>

      <!--notes 1 to 4 
      -->

      <xsl:choose>
        <xsl:when test="./@freq"><xsl:value-of select="./@freq" /></xsl:when>
        <xsl:otherwise>
          <xsl:value-of select="../../../def/@freq" />
        </xsl:otherwise>
      </xsl:choose>

      <xsl:copy-of select="$CSEP" />
      <xsl:copy-of select="$CSEP" />
      <xsl:copy-of select="$CSEP" />
      <xsl:copy-of select="$CSEP" />

      <!--text not available -->
      <xsl:for-each select="(2 to 4)">
        <xsl:copy-of select="$CSEP" />
      </xsl:for-each>

      <!--examples 1 to 4 available -->
      <xsl:for-each select="(1 to 4)">
        <xsl:copy-of select="$CSEP" />
      </xsl:for-each>

      <xsl:variable name="ExCount1" select="count(def/ex/ex_orig[text()])"/>
      <xsl:for-each select="def">

        <xsl:for-each select="ex">
          <!--  add an if statement to not insert null values; but this will throw out the 18x calc -->

          <xsl:choose>
            <xsl:when test="ex_orig != ''" >

              <xsl:value-of select="normalize-space(ex_orig)"/>
              <xsl:copy-of select="$CSEP" />
              <xsl:value-of select="normalize-space(ex_tran)"/>
              <xsl:copy-of select="$CSEP" />
            </xsl:when>
            <xsl:otherwise>
            </xsl:otherwise>
          </xsl:choose>


        </xsl:for-each>

      </xsl:for-each>

      <xsl:for-each select="(1 to 18 - $ExCount1)">
        <xsl:copy-of select="$CSEP" />
        <xsl:copy-of select="$CSEP" />
      </xsl:for-each>

      <!--etm -->
      <xsl:copy-of select="$ExCount1" />
      <xsl:copy-of select="$CSEP" />

      <!-- tags -->
      <xsl:for-each select="def">
        <xsl:if test="count(categ) > 0 " >
          <xsl:for-each select="categ">
            <xsl:value-of select="." />
            <xsl:value-of select="$delim" />
            <xsl:if test="position() != last()" >
            </xsl:if>
            <xsl:if test="following-sibling::categ" >
            </xsl:if>
          </xsl:for-each>
        </xsl:if>
        <xsl:if test="following-sibling::def" >
        </xsl:if>
      </xsl:for-each>

      <xsl:text> </xsl:text>

      <xsl:copy-of select="$RSEP" />

    </xsl:for-each>
  </xsl:for-each>
</xsl:template>

</xsl:stylesheet>

