#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

project=project
file=dictd.xml


#[ -f ${HEADWORDS} ] || { echo "${HEADWORDS} not found.  Exiting..."; exit 1; }

for x in tmp xml xsd yaml adoc json; do export "${x}=${project}.$x"; done

for x in sed ; do
type -P $x >/dev/null 2>&1 || { echo >&2 "${x} not installed.  Aborting."; exit 1; }
done

xslt3 -xsl:lexicon.xsl -s:lexicon.xml > $file
sed -i '2 i <!DOCTYPE xdxf SYSTEM "xdxf_strict.dtd">' $file

