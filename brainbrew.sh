#!/usr/bin/bash
set -o errexit
set -o nounset
set -o pipefail

echo "Update brainbrew"
update=/home/bkelly/Anki/brainbrew/Jula/build/Jula/
brainbrew=/home/bkelly/Anki/brainbrew/Jula
cp -v ./build/lexicon.tsv $brainbrew/src/data/

pushd $brainbrew || exit
./upd-headwords-step1.sh french && ./upd-step2.sh
popd || exit
git -C $update diff-index --quiet HEAD -- || git -C $update commit -a -m "updated on: $(date) by $0"
git -C $update push origin main

# update=/home/bkelly/Anki/brainbrew/Examples/build/Examples/
# brainbrew=/home/bkelly/Anki/brainbrew/Examples/
# git -C $update pull origin main
# pushd $brainbrew && brainbrew run recipes/source_to_anki.yaml && popd
# git -C $update diff-index --quiet HEAD -- || git -C $update commit -a -m "updated on: $(date) by $0"
# git -C $update push origin main
