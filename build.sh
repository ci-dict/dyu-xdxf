#!/usr/bin/bash
#set -o errexit
set -o nounset
set -o pipefail

for x in jq yq csvclean brainbrew pyglossary; do
  type -P $x >/dev/null 2>&1 || {
    echo >&2 "${x} not installed.  Aborting."
    exit 0
  }
done

set -o errexit
file=lexicon
project=mandenkan
build=build
lib=lib
src=src
dict=./$build/dict.xdxf
branch=dev
dest="$HOME"/dev/svelte/coastsystems.net
dyu_lex="$HOME"/dev/svelte/dyu-lex
for x in tsv tmp xml xsd xsl adoc json yml md slob; do export "${x}=${file}.$x"; done
source ./functions.sh

[[ -d $build ]] && rm -fr $build
[[ -d $build ]] || mkdir $build

checkbranch "$dest" "$branch"
checkbranch "$dyu_lex" "$branch"

for file in src/xdxf*; do ln -sf "$PWD/$file" build/; done

[[ -f "$build/$xml" ]] && count_xml "$build/$xml"

#make xml (and json)
DATE=$(date -I) && export DATE
yq -i '.xdxf.meta_info.last_edited_date = strenv(DATE)' "$yml"
yamlfmt "$yml"

yq -oj "$yml" >"$build/$json"
yq -ox "$yml" >"$dict"
ln -srvf $src/xdxf_strict.dtd $build/
xmllint --noout --dtdvalid xdxf_strict.dtd ./$dict
ln -vf $dict "./$build/$xml"
#now we have a new xml file.  sort it back to the yaml
npx xslt3 -xsl:$src/$project-sort.xsl -s:"$build/$xml" >"~sorted-$xml"

yq -px -oy '
  .xdxf.lexicon.ar[].def |= ([] + .) |
  .xdxf.lexicon.ar[].def[].def |= ([] + .) |
  .xdxf.lexicon.ar[].def[].def[].def |= ([] + .) |
  .xdxf.lexicon.ar[].def[].def[].def[].def |= ([] + .) |
  .xdxf.lexicon.ar[].k |= ([] + .) |
  .xdxf.lexicon.ar[].def[].def[].def[].gr |= ([] + .) |
  .xdxf.lexicon.ar[].def[].def[].def[0].def[].ex |= ([] + .)
' "~sorted-$xml" >"new-$yml"

count_xml "$build/$xml"
count_xml "~sorted-$xml"
count_yml "$yml"
sed -i "2i +directive: DOCTYPE xdxf SYSTEM 'xdxf_strict.dtd'" "new-$yml"
cp -v "$yml" "./archive/$(date -Im)-$yml"
mv -v "new-$yml" "$yml"
count_yml "$yml"
yamlfmt "$yml"
rm ./~*

#make lexicon specific
for x in lexicon phrases animal.fr animal.dyu; do
  npx xslt3 -xsl:$src/$x.xsl -s:$dict >$build/$x.tsv
  sleep 1
done

csvtool -t TAB -u COMMA cat $build/lexicon.tsv >$build/lexicon.csv

npx xslt3 -xsl:$src/lexicon-adoc.xsl -s:"$build/$xml" >$build/vocab-dyu.adoc
npx xslt3 -xsl:$src/xdxf-html.xsl -s:$build/$xml |
  npx prettier --parser html |
  sed -e 's/ / /g' \
    >"$dyu_lex"/src/routes/dyu/Dioula.svelte

#make web metadata
#removes 'null' from records (I think there is also another better way with yq?)
npx xslt3 -xsl:$src/animal-xml.xsl -s:"$build/$xml" | yq -px -oj -I0 | sed 's/null/\"\"/g' >$build/animal.json
# i think this is traversing the structure and converting anthing not a string to a string?? (for charts)
npx xslt3 -xsl:$src/w100-xml.xsl -s:"$build/$xml" | yq -px -oj '(.. | select(tag == "!!str")) |= from_yaml' | sed 's/null/\"\"/g' >$build/w100-kodi.json
# for the time being...
npx xslt3 -xsl:$src/dyu-refs.xsl -s:"$build/$xml" | sort -u -t, -k1,1 >$build/dyu-refs.csv
npx xslt3 -xsl:$src/dyu-links.xsl -s:"$build/$xml" | sort -u -t, -k1,1 >$build/dyu-links.csv
npx xslt3 -xsl:$src/dyu-uri.xsl -s:"$build/$xml" | sort -u -t, -k1,1 >$build/dyu-uri.csv
# npx xslt3 -xsl:dyu-syn.xsl -s:"$build/$xml" | sort -u -t, -k1,1 >$build/dyu-syn.csv
# validate $build/dyu-syn.csv

#make logseq/obsidian
npx xslt3 -xsl:$src/logseq.xsl -s:"$build/$xml" >~/notes/logseq/lexique/pages/Index.md
npx xslt3 -xsl:$src/obsidian.xsl -s:"$build/$xml" >~/notes/obsidian/dioula/lexique/Index.md

# make neo4j
for x in hw def ex r_means r_examples r_usage hw_keys; do
  npx xslt3 -xsl:$src/neo_$x.xsl -s:"$build/$xml" >$build/neo_$x.tsv
done
sort -u $build/neo_hw_keys.tsv -o $build/neo_hw_keys.tsv

#lets check these before copying
for x in "$build"/*tsv; do validate "$x"; done
#for x in "$build"/*csv; do validate "$x"; done

echo "update neo4j import data"
targetdir="$HOME/dev/neo4j/import"
[[ -d "$HOME/dev/neo4j/import" ]] && sudo cp -v "./$build"/neo_*.tsv "$targetdir/"

echo "update svelte metadata"
targetdir="$HOME/dev/svelte/coastsystems.net/src/_include"
#cp -v $build/animal.fr.tsv $targetdir/fr/animal.tsv
#cp -v $build/animal.dyu.tsv $targetdir/dyu/animal.tsv

targetdir="$dest/src/assets/metadata"
cp -v $build/w100-kodi.json $targetdir/
cp -v $build/animal.json $targetdir/

#note the name change
#make Anki & brainbrew
cp -v $build/lexicon.tsv /home/bkelly/Anki/brainbrew/Jula/src/data/update.tsv
cp -v $build/phrases.tsv /home/bkelly/Anki/brainbrew/Examples/src/data/Phrases.tsv
cp -v $build/phrases.tsv /home/bkelly/dev/jula/slides-dyu/

#git diff-index --quiet HEAD -- || git commit -a -m "updated on: $(date) by $0"
#git push origin main

# git -C /home/bkelly/dev/web-docs submodule update --remote
# git -C /home/bkelly/dev/web-docs commit -a -m "updated on: $(date) by $0"

#make pyglossary
[[ -f "$build/$slob" ]] && rm "$build/$slob"
pyglossary "$dict" "$build/$slob"
cp -v "$build/$slob" "$HOME/drive/Sync/apk/"

echo "update slides"
targetdir="/home/bkelly/dev/jula/slides-dyu"
cp -v $build/animal.dyu.tsv $targetdir/animal.dyu.tsv
git -C $targetdir diff-index --quiet HEAD -- || git -C $targetdir commit -a -m "updated on: $(date) by $0"
git -C $targetdir push origin main
